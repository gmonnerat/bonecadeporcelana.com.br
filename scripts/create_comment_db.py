import sqlite3

def main():
    conn = sqlite3.connect('comment.db')
    cursor = conn.cursor()
    # Create table
    cursor.execute('''CREATE TABLE comments
              (id text,
              url text,
              sender text,
              message text,
              creation_date text)''')
    conn.commit()
    conn.close()

if "__main__" == __name__:
    main()
