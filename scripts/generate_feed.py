# encoding: utf-8
import datetime
import feedparser
import PyRSS2Gen

def getDescription(entry):
  return "\n".join(entry['description'].split('\n')[:2]) + \
    "<p><a href='%s'>Continue Lendo</a></p>" % entry['link']

def g(entry):
  return PyRSS2Gen.RSSItem(title=entry['title'],
    guid=entry['id'], link=entry['link'],
    pubDate=entry['published'],
    categories=[e['term'] for e in entry['tags']],
    description=getDescription(entry))

tumblr_feed = feedparser.parse('http://www.bonecadeporcelana.com.br/rss')
feed = tumblr_feed['feed']
rss = PyRSS2Gen.RSS2(
    title = feed['title'],
    link = feed['link'],
    description = feed['subtitle'],
    items = [g(entry) for entry in tumblr_feed['entries']],
)

rss.write_xml(open("/data/home/pi/rss/rss.xml", "w"))
