import json

# Make the request
limit = client.blog_info('marcelamonnerat.tumblr.com').get('blog').get('posts')
index = 30

br_post_per_slug = {}
fr_post_per_slug = {}

for offset in xrange(0, limit, index):
  results = client.posts('marcelamonnerat.tumblr.com', limit=index, offset=offset)
  for post in results.get('posts'):
    br_post_per_slug[post.get('slug')] = post

limit = client.blog_info('marcelamonneratfr.tumblr.com').get('blog').get('posts')
for offset in xrange(0, limit, index):
  results = client.posts('marcelamonneratfr.tumblr.com', limit=index, offset=offset)
  for post in results.get('posts'):
    fr_post_per_slug[post.get('slug')] = post

not_found_list = []
json_dict = {}
for slug in br_post_per_slug.iterkeys():
  if slug in fr_post_per_slug:
    json_dict[br_post_per_slug[slug].get('id')] = \
        fr_post_per_slug[slug].get('post_url')
    json_dict[fr_post_per_slug[slug].get('id')] = \
        br_post_per_slug[slug].get('post_url')
  else:
    not_found_list.append(slug)

open("urls.json", "wb").write(json.dumps(json_dict))
print "\n".join(not_found_list)
