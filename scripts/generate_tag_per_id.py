import sys
import os.path
from json import dumps

BASE_URL = "https://bytebucket.org/gmonnerat/bonecadeporcelana.com.br" + \
        "/raw/593f763f282f3716544dba994874978d1787edfe/images/"

# Make the request
limit = client.blog_info('marcelamonnerat.tumblr.com').get('blog').get('posts')
index = 30

br_post_per_slug = {}
fr_post_per_slug = {}

for offset in xrange(0, limit, index):
  results = client.posts('marcelamonnerat.tumblr.com', limit=index, offset=offset)
  for post in results.get('posts'):
    br_post_per_slug[post.get('slug')] = post

limit = client.blog_info('marcelamonneratfr.tumblr.com').get('blog').get('posts')
for offset in xrange(0, limit, index):
  results = client.posts('marcelamonneratfr.tumblr.com', limit=index, offset=offset)
  for post in results.get('posts'):
    fr_post_per_slug[post.get('slug')] = post

not_found_list = []
json_dict = {}
id_list = [int(r.strip()) for r in open('consider.txt').readlines()]
for slug in br_post_per_slug.iterkeys():
  if slug in fr_post_per_slug:
    post_id = br_post_per_slug[slug].get('id')
    if post_id in id_list:
      json_dict[fr_post_per_slug[slug].get('id')] = BASE_URL + str(post_id) + ".jpg"
  else:
    not_found_list.append(slug)



post_per_id = {}

for post in fr_post_per_slug.itervalues():
  if post.get('id') not in json_dict:
    #print "%s Ignored" % post.get('post_url')
    continue
  for tag in post.get('tags'):
    if any(i in tag for i in [' ', '-', '&']) or \
            tag.encode("ascii", "ignore") != tag:
      continue
    print tag, post.get('slug')
    post_per_id.setdefault(tag, []).append(
      [json_dict[post.get('id')],
        post.get("post_url"),
        post.get("title").encode('utf-8').strip()])

print post_per_id
open("data.js", "wb").write("var tag_data = JSON.parse('%s');" % dumps(post_per_id))
