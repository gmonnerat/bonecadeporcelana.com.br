# -*- coding: utf-8 -*-

import os
import pytumblr
import yaml
import urllib2
import sqlite3
import smtplib
import string
from json import loads
from pprint import pformat

FACEBOOK_URL = "https://graph.facebook.com/comments/?ids="
OPENER = urllib2.build_opener()


def main():
    yaml_path = os.path.expanduser('~') + '/.tumblr'
    yaml_file = open(yaml_path, "r")
    tokens = yaml.safe_load(yaml_file)
    yaml_file.close()

    client = pytumblr.TumblrRestClient(
      tokens['consumer_key'],
      tokens['consumer_secret'],
    )

    # Make the request
    limit = client.blog_info('marcelamonnerat.tumblr.com').get('blog').get('posts')
    index = 30

    url_list = []

    for offset in xrange(0, limit, index):
      results = client.posts('marcelamonnerat.tumblr.com', limit=index, offset=offset)
      for post in results.get('posts'):
        url_list.append(post.get('post_url'))

    limit = client.blog_info('marcelamonneratfr.tumblr.com').get('blog').get('posts')
    for offset in xrange(0, limit, index):
      results = client.posts('marcelamonneratfr.tumblr.com', limit=index, offset=offset)
      for post in results.get('posts'):
        url_list.append(post.get('post_url'))

    data_dict = {}
    for post_url in url_list:
        url = "%s%s" % (FACEBOOK_URL, post_url)
        request = urllib2.Request(url)
        response = OPENER.open(request)
        post = loads(response.read())
        for blog_url, comment_dict in post.iteritems():
            data_dict[blog_url] = \
                    comment_dict.get("comments", {}).get('data', [])

    conn = sqlite3.connect("/home/gabriel/bonecadeporcelana.com.br/scripts/comment.db")
    cursor = conn.cursor()
    id_list = [cid[0] for cid in cursor.execute('SELECT id from comments').fetchall()]
    new_messages = {}
    for url, data in data_dict.iteritems():
        for comment in data:
            cid = comment.get('id').encode('utf-8')
            if cid not in id_list:
                message = comment.get("message", '').encode('utf-8')
                name = comment.get('from', {}).get('name', '').encode('utf-8')
                cursor.execute("INSERT INTO comments VALUES " + \
                    "('%s', '%s', '%s', 'null', 'null')" % (cid,
                        url.encode('utf-8'), name))
                new_messages.setdefault(url, []).append((name, message))
    conn.commit()
    conn.close()

    fromaddr = 'noreply@bonecadeporcelana.com.br'
    toaddrs  = 'contatobporcelana@gmail.com'
    if new_messages:
        msg = "Comentarios para responder:\n\n"
        for url, comments in new_messages.iteritems():
            msg += "%s\n%s\n\n" % (url.encode('utf-8'),
                    "\n".join(["%s %s" % comment for comment in comments]))

        # Credentials (if needed)
        username = 'marcelamonnerat1987@gmail.com'
        password = open('/home/gabriel/.gmail.pwd', "wb").read()
        BODY = string.join((
            "From: %s" % fromaddr,
            "To: %s" % toaddrs,
            "Subject: Novos comentarios",
            "",
            msg
            ), "\r\n")
        # The actual mail send
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(username, password)
        print "Sending email..."
        server.sendmail(fromaddr, toaddrs, BODY)
        server.quit()

if "__main__" == __name__:
    main()
